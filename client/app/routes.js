var app = angular.module('appRoutes', ['ngRoute'])

.config(function($routeProvider,$locationProvider){
   
    $routeProvider

    .when('/', {
        templateUrl: 'app/views/home.html'

    })

    .when('/about', {
        templateUrl: 'app/views/about.html'
    })

    .when('/register', {
        templateUrl: 'app/views/users/register.html',
        controller: 'regCtrl',
        controllerAs: 'register',
        authenticated: false
    })

    .when('/login', {
        templateUrl: 'app/views/users/login.html',
        authenticated: false

    })

    .when('/logout', {
        templateUrl: 'app/views/users/logout.html',
        authenticated: true
    })

    .when('/profile', {
        templateUrl: 'app/views/users/profile.html',
        authenticated: true
    })
    
    .when('/facebook/:token', {
        templateUrl: 'app/views/users/social/social.html',
        controller:'facebookCtrl',
        controllerAs: 'facebook',
        authenticated: false
    })

    .when('/facebookerror', {
        templateUrl: 'app/views/users/login.html',
        controller:'facebookCtrl',
        controllerAs: 'facebook',
        authenticated: false
    })

    .otherwise({redirectTo: '/'});

    $locationProvider.html5Mode({
        enabled:true,
        requiredBase: false
    });
});

app.run(['$rootScope', 'Auth', '$location',  function($rootScope, Auth, $location){

    $rootScope.$on('$routeChangeStart', function(event, next, current) {

        if(next.$$route.authenticated == true) {
            if(!Auth.isLoggedIn()){
                event.preventDefault();
                $location.path('/');
            }
        } else if (next.$$route.authenticated == false) {
            if(Auth.isLoggedIn()) {
                event.preventDefault();
                $location.path('/profile')
            }
      
        } else {
           
        }

        console.log(next.$$route.authenticated);
    })
}]);


