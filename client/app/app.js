angular.module('userApp',['appRoutes', 'usersControllers', 'userServices','ngAnimate', 'mainController','authServices'])

.config(function($httpProvider){
    $httpProvider.interceptors.push('AuthInterceptors')
});